IMAGE_NAME ?= nexylan/release
IMAGE_TAG ?= latest

all: build

build:
	docker build --tag $(IMAGE_NAME):$(IMAGE_TAG) .
