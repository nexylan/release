# Summary

<!-- Describe the bugs in a few concise words. -->

## Steps to reproduce

<!--
  Examples MUST be replaced by your workflow.
-->

1. Go to page...
1. Click on...
1. Run following command: `...`

## What is the current bug behavior?

<!--
  Describe what you expect to see.
-->

## What is the expected correct behavior?

<!--
  Describe what happened instead.
-->

## Relevant information

<!--
  Put anything you find useful to help resolving this bug.

  Can be environnement info (OS, browser, product version...), log trace, screenshot or anything else.
-->

N/A

# Bug resolution

## Possible workarounds

<!--
  How can we get around of this bug? Leave blank if no workaround found.
-->

N/A

## Possible fixes

<!--
  How can we fix this bug? Leave blank if no idea.
-->

N/A

/label ~"type::bug"
