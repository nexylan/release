#!/usr/bin/env sh

# It is currently impossible to directly specify a config path.
cp /release.config.js .

npx semantic-release "$@"
