# Release

Semantic release utility.

The tool: https://semantic-release.gitbook.io

## Commit rules

The convention we follow: https://conventionalcommits.org

All the documentation is present on this link, but you can also read the résumé below.

You may also read the [semantic-release documentation][semantic_release_message_format]
about commit message format. It also links tools to help building commit message interactively.

### Format

The commit **title** must follow the following format: `type(scope): <description>`.

The commit title can not exceed **72 characters**. Use the commit body to add details.

#### Type

In lowercase only, you can choose the following type according to the nature of the commit:

- `fix`: When you fix something. Will make a patch release (1.0.Z).
- `feat`: When you introduce a new feature or improvements. Will make a minor release (1.X.0). There is some special cases according to [semver specification][semver_website]:
  - When you drop a vendor related version, it should be done on a minor release.
- `revert`: Just use the `git revert` command and leave the generated message as is. It will generate a patch release (1.0.Z) (see capture below).

Revert process example:

![revert_process](https://gitlab.com/nexylan/release/uploads/47cb7e41441d0a94c0d580abbf10641e/image.png)

Those are the two type you will have to use if you need a release and production deployment.

The other type described below concerns changes that does not need a new production release, but may help:

- `docs`: When you change something on the project documentation (e.g. markdown files, code comments...).
- `refactor`: When you change the project codebase to improve maintenance and readability, without affecting the meaning of the code (e.g. Code duplicate removal).
- `test`: When you add ore modify some tests.
- `ci`: Change that affect CI configuration (e.g. GitLab CI, Travis...). ⚠️ If the CI change fix a **production** deployment, you must use `fix` instead.
- `build`: Used for build system or external dependencies change. A discussion is opened about creating a fourth release level for that: https://gitlab.com/nexylan/release/-/issues/1
  Generally used by the bot.
- `chore`: When your changes are not related to any of the other types.

Those final types described below are not really recommended to be used. It concerns code improvement, but does not create a release. This might not be what you want:

- `improvement`: An improvement to a current feature.
- `perf`: Like `improvement` but for performance only.

However, the release tool is configurable and we may improve it to make new release using this kind of type.
Please open an issue for discussion if you need one of these.

##### The revert case

Assuming you have this git log history:

```
❯ git log --oneline
5458009 (HEAD -> master, tag: v1.2.0) feat: blog post comment
499bbfb (tag: v1.1.0) feat: blog post
6c0fa68 (tag: v1.0.0) feat: setup
```

You recently push a new minor release with blog post comment, but you found a critical high security issue and you don't know how to fix it quickly.

⚠️ You may prefer a deployment rollback then a fix addition than a revert.
A revert will "cancel" the work done and will have to be entirely submitted again with a new merge request.
Sometime, the fix can be done quiclky and a simple update of the project
Also, deployment rollback are quicker to execute than a complete build.

#### Scope

The scope is optional but may help you to organize your changelog and simplify your commit title.

For example, if you have a blog project and you fix the fact any user can login as an administrator, your commit can have this form:

```
fix(security): avoid basic user to login as administrator
```

Or, if you upgrade you want to drop a PHP version of your library:

```
feat(php): drop version 5.6 support
```

The scope usage and definition heavily depends of your project structure. There is no global rule to follow.

#### Description

The commit description must be short and concise.

It must describe what you fixed/added (according to the user target), not what you did.
The commit diff is here for that.

For example:

```
# bad commit.
fix: add permissions to admin/index.php

# good commit.
fix: server crash on admin homepage

# bad commit.
feat: create CommentType.php

# good commit.
feat(blog): comments system
```

If you are not sure of what to put on the description,
keep in mind this will appear on the release changelog,
so it must be understandable to the product end user.

#### Commit body

You MUST add a blank line between the commit title and the body:

```
fix(foo): bar

This is a commit description.

It prevents google to be angry.

Ref: https://google.com/i-am-angry
```

Otherwise, you can put anything you want on the body.

This is useful to add some details about what you did and why and put some references.

This also allows you to keep the commit title short:

```
# This:
fix: use correct permissions for /foo/bar to prevent crashes on ubuntu 16.04

# May be turned to this:
fix: use correct permissions for /foo/bar

Prevents crashes on Ubuntu 16.04 and upper.

Ref: https://ubuntu.org/changelog/16-04#file-permissions
```

#### Breaking Change

As `fix` and `feat` type produces respectively patch and minor releases, the major release (X.0.0) does not have a type.

You MUST create a new major release when any change is breaking the end usage of your product.

In that case, you have to put this at the end of the commit body: `BREAKING CHANGE: short description of the change.`.

This will produce a new major release, regardless of the commit type and add the breaking change note to the changelog.

For example, if you have a product using `product.yml` and you change one of the configuration key:

```
feat(user): allow to configure multiple emails

BREAKING CHANGE: `user.email` key has been renamed to `user.emails`.
```

Note: Major release should be avoided as possible. Instead, try to keep backward compatibility by deprecating features/options.
You may create a major release later by removing all the deprecated stuff.
Here is a good example with the Symfony project: https://symfony.com/doc/current/contributing/community/releases.html#backward-compatibility

## Usage

This tool is supposed to be used on CI only.

### GitLab CI

```yaml
release:
  stage: release
  # You should lock to a specific verion below.
  image:
    name: registry.gitlab.com/nexylan/release
    entrypoint: [""]
  # Make sure no service is activated to speed up the CI job.
  services: []
  script: release
  only:
    - master
```

[semver_website]: https://semver.org/ "The semver website"
[semantic_release_message_format]: https://semantic-release.gitbook.io/semantic-release/#commit-message-format "Commit message format"
