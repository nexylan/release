const config = {
  branches: process.env.CI_DEFAULT_BRANCH || 'main',
  plugins: [
    ['@semantic-release/commit-analyzer', {
      preset: 'conventionalcommits',
    }],
    '@semantic-release/release-notes-generator',
  ],
};

if (typeof process.env.GITLAB_CI !== 'undefined') {
  config.plugins.push('@semantic-release/gitlab');
}

module.exports = config;
