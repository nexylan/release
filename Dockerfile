FROM node:12-alpine

RUN apk add --no-cache git~=2

RUN npm install --global \
  semantic-release@17.4.3 \
  conventional-changelog-conventionalcommits@4.6.0 \
  @semantic-release/commit-analyzer@8.0.1 \
  @semantic-release/release-notes-generator@9.0.2 \
  @semantic-release/gitlab@6.0.9

COPY release.config.js /

COPY release.sh /bin/release
RUN chmod +x /bin/release

ENTRYPOINT [ "release" ]
